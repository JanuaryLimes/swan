module.exports = {
  schema: [
    {
      'https://hasura-endpoint.herokuapp.com/v1/graphql': {
        headers: {
          'x-hasura-admin-secret': `${process.env.ADMIN_TOKEN}`,
        },
      },
    },
  ],
  documents: [
    './components/**/*.tsx',
    './components/**/*.ts',
    './pages/**/*.tsx',
    './pages/**/*.ts',
  ],
  overwrite: true,
  generates: {
    './graphql-generated/graphql.tsx': {
      plugins: [
        'typescript',
        'typescript-operations',
        'typescript-react-apollo',
      ],
      config: {
        skipTypename: false,
        withHooks: true,
        withHOC: false,
        withComponent: false,
      },
    },
    './graphql.schema.json': {
      plugins: ['introspection'],
    },
  },
};
