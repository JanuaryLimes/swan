import * as React from 'react';
import { Product } from '../../graphql-generated/graphql';

type ItemProps = {
  product: Partial<Product>;
};

export const Item: React.FC<ItemProps> = ({ product }) => {
  return (
    <div className="w-full rounded p-2 bg-blue-100">
      <div>// TODO image</div>
      <div>{product.name}</div>
      {product.description && <div>{product.description}</div>}
      <div>${product.price}</div>
    </div>
  );
};
