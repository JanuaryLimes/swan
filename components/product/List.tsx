import * as React from 'react';
import { Product } from '../../graphql-generated/graphql';
import { Item } from './Item';

type ListProps = {
  products: Partial<Product[]>;
};

export const List: React.FC<ListProps> = ({ products }) => {
  // TODO case when no items

  return (
    <div className="flex justify-center">
      <div className="flex flex-wrap w-full max-w-5xl">
        {products.map((product) => (
          <Card key={product?.id}>
            <Item product={product ?? {}} />
          </Card>
        ))}
      </div>
    </div>
  );
};

const Card: React.FC = ({ children }) => {
  return (
    <div className="flex w-full sm:w-1/2 lg:w-1/3 xl:w-1/4 p-1">{children}</div>
  );
};
