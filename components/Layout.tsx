import * as React from 'react';
import Link from 'next/link';
import Head from 'next/head';
import { Affix } from 'antd';
import { blue } from '@ant-design/colors';

type Props = {
  title?: string;
};

export const Layout: React.FunctionComponent<Props> = ({
  children,
  title = 'swan.'
}) => (
  <div>
    <Head>
      <title>{title}</title>
      <meta charSet="utf-8" />
      <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    </Head>
    <Affix>
      <header className="p-2 text-xl" style={{ background: blue[0] }}>
        <nav>
          <Link href="/">
            <a>swan.</a>
          </Link>{' '}
          |{' '}
          <Link href="/about">
            <a>About</a>
          </Link>{' '}
          |{' '}
          <Link href="/users">
            <a>Users List</a>
          </Link>
        </nav>
      </header>
    </Affix>
    <main className="p-2" style={{ minHeight: '80vh' }}>
      {children}
    </main>
    <footer className="p-2 pb-4">
      <hr className="pb-2" />
      <span>I'm here to stay (Footer)</span>
    </footer>
  </div>
);
