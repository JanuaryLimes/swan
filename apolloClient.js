import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { HttpLink } from 'apollo-link-http';

export default function createApolloClient(initialState, ctx) {
  // The `ctx` (NextPageContext) will only be present on the server.
  // use it to extract auth headers (ctx.req) or similar.
  return new ApolloClient({
    ssrMode: Boolean(ctx),
    link: new HttpLink({
      uri: 'https://hasura-endpoint.herokuapp.com/v1/graphql', // Server URL (must be absolute)
      headers: {
        'x-hasura-admin-secret': `${process.env.ADMIN_TOKEN}`, // TODO
        Authorization: `Bearer ${process.env.BEARER_TOKEN}`,
      },
    }),
    cache: new InMemoryCache().restore(initialState),
  });
}

// TODO bearer from auth token
