import * as React from 'react';
import { Layout } from '../components/Layout';
import { NextPage } from 'next';
import { withApollo } from '../lib/apollo';
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { AllProductsQuery, Product } from '../graphql-generated/graphql';
import { List as ProductList } from '../components/product/List';

export const ALL_PRODUCTS_QUERY = gql`
  query allProducts {
    product {
      id
      name
      price
      description
      created_at
    }
  }
`;

type Props = {};

const domain = `https://swan-io.eu.auth0.com`;
const client_id = `9hYLBLigsZZmAdfq3z9TQTBIT0uyzYmA`;
const callback_uri = `http://localhost:3000/callback`;

const loginUrl = `${domain}/login?client=${client_id}&protocol=oauth2&response_type=token%20id_token&redirect_uri=${callback_uri}&scope=openid%20profile`;
// TODO auth0 hook

const IndexPage: NextPage<Props> = ({}) => {
  const { loading, error, data } = useQuery<AllProductsQuery>(
    ALL_PRODUCTS_QUERY
  );

  console.warn('query result?', {
    loading,
    error,
    data,
  });

  return (
    <Layout title="Home | swan.">
      {/* <h1>Hello Next.js 👋</h1> */}
      {/* 
      <div className="p-2">
        <a href={loginUrl}>Login</a>
      </div> */}
      {/* <p>
        <Link href="/about">
          <a>About</a>
        </Link>
      </p> */}
      <div>
        <ProductList products={(data?.product as Product[]) ?? []} />
      </div>
    </Layout>
  );
};

export default withApollo({ ssr: true })(IndexPage);
