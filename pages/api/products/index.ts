import { NextApiRequest, NextApiResponse } from 'next';

type Product = {
  name: string;
};

export default (_: NextApiRequest, res: NextApiResponse) => {
  try {
    const products: Product[] = [
      { name: 'item 1' },
      { name: 'item 2' },
      { name: 'item 3' },
      { name: 'item 4' }
    ];

    res.status(200).json({ products });
  } catch (err) {
    res.status(500).json({ statusCode: 500, message: err.message });
  }
};
